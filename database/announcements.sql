-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 15, 2011 at 07:25 PM
-- Server version: 5.1.36
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `announcements`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminacc`
--

CREATE TABLE IF NOT EXISTS `adminacc` (
  `username` varchar(20) NOT NULL,
  `pass` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminacc`
--

INSERT INTO `adminacc` (`username`, `pass`) VALUES
('admin', 'demo');

-- --------------------------------------------------------

--
-- Table structure for table `currentan`
--

CREATE TABLE IF NOT EXISTS `currentan` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `text` varchar(200) NOT NULL,
  `active` int(1) NOT NULL,
  `Uptime` int(3) NOT NULL,
  `Importance` int(3) NOT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `currentan`
--

INSERT INTO `currentan` (`id`, `text`, `active`, `Uptime`, `Importance`) VALUES
(63, 'The server will be down in 5 mins, Save your work!', 1, 5, 2);
