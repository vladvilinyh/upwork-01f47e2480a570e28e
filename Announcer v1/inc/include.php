<?php require_once("config.php"); ?>

<!-- including The javascript files -->

<script type="text/javascript" src="resources/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="resources/js/announcer.js"></script>

<!-- Including the CSS stylesheet file for the bar -->

<link rel="stylesheet" type="text/css" href="resources/css/announcer.css" />

<!-- calling the ajax function that brings the news on realtime -->
<script type="text/javascript">


$(document).ready(function(){

	$(document).mkannouncer({
		
		// the options of the bar
		easy_color:'#0CF', 				// the color of the normal level notification
		moderate_color:'#c3f243',		// the color of the Moderate level notification
		critical_color:'#F30',			// the color of the Critical level notification
		update_request_timeout:1000		// the time between two successive update requests (recommended to leave it 1000)
		
	});
	
});


</script>

