// Ajax js to get the updated notifications.


$(function(){
	$.fn.extend({

	mkannouncer : function(options){ 

	var defaults = {
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///// the following lines 15-20 have the default values (that will be activated if not set in the function call in the include.php file)/////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		idle_color:'#444', // the default color for the bar when there are no news active.
		easy_color:'#0CF', // the color of the normal level news
		moderate_color:'#c3f243', // the color of the moderate level news
		critical_color:'#F30', // the color of the critical level news
		font_color:'#fff', // the font color of the text inside the bar
		update_request_timeout:1000 // the time between each successive updates (it is RECOMMENDED to leave it unchanged)
		
	};
	
	var options = $.extend(defaults,options);
		
	return this.each(function() {

// the global and cashing variables

var o = options;
		
var http = false;
var timeout;
var count = 0;
var id=0;
var notifull = new Array();
var notif = new Array();
var notiftime = new Array();
var notifimp = new Array();
var start = false;
var pinon = true;
var sticky = true;
var $bar;

// on page load
	$(window).load(function()
	{	
		

		// the following line creates the bar html code and adds it to the page
		$('body').append("<div id=\"mkbar\"> <div id=\"loading\"> </div> <div id=\"mkpin\"> </div> <div id=\"notf\">  </div> </div>");
		

		$bar = $('#mkbar');
		
		$('#notf').css('color',o.font_color);
		$bar.css('backgroundColor',o.idle_color);
				
		// calls the function that retreives the announcemenet via ajax call
		getannouncement();
		
		// calls the function that writes the announcements in order to the bar
		writeannouncement(0);
		
		
		
		
$('#mkpin').click(function()
{
	if(pinon)
	{
			$(this).css('backgroundImage','url(resources/images/pinoff.png)');
			sticky = false;
			pinon = false;
	}
	else
	{
			$(this).css('backgroundImage','url(resources/images/pinon.png)');
			sticky = true;
			pinon = true;
	}
	
});


// the following functions are active when the bar is unpinned
$bar.mouseleave(function()
{
	if(!pinon)
	{
			$(this).stop().animate({top:-20},500); // -20 is the vertical position of the bar when the mouse leaves it
	}

	
});

$bar.mouseenter(function()
{
	if(!pinon)
	{
			$(this).stop().animate({top:0},500); // 0 is the vertical position of the bar when the mouse is on it
	}

	
});

	});





function createXMLHttpRequest() {
 return( window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest( ) );
 };


function getannouncement() {



  var timeout = setTimeout(function() {
	
  var http = createXMLHttpRequest();

  http.abort();
  http.open("POST", "resources/php/announce.php", true);
  http.onreadystatechange=function(){
    if(http.readyState == 4) {
	
		
	
		var str = http.responseText;
		


	  	count++;
	  	id++;
		
		// the following part divides the retrieved string according to the delimeters
		
		str = str.substring(0,str.length-2);
	
		notifull = str.split('...!!!...');
		
		var notiftmp = new Array();
		var notiftmp2 = new Array();
		var notiftmp3 = new Array();
		
		
		
		for(var i1=0;i1<notifull.length;i1++)
		{
			var notifent = 	notifull[i1].split('..!!..');
			notiftmp[i1]=(notifent[0]);
			notiftmp2[i1]=(notifent[1]);
			notiftmp3[i1]=(notifent[2]);
			
		}
		
		
		
		notif = notiftmp;
		notiftime = notiftmp2;
		notifimp = notiftmp3;
		
		start = true;
		
		
		
		
    }
  }
  
  http.send(null);
  getannouncement();
  
  
  
 },o.update_request_timeout);

}

var st = false;
var curt=1000;

function writeannouncement(ind)
{
	
	
		
	if(start)
	{
	$bar.css('backgroundColor',o.idle_color);
		$('#loading').hide();
		
		
		
		
		$('#mkbar #notf').html(notif[ind]);
		
		// set the color according to the settings
		if(notifimp[ind]==1)
			$bar.css('backgroundColor',o.easy_color);
		else if(notifimp[ind]==2)
			$bar.css('backgroundColor',o.moderate_color);
		else if(notifimp[ind]==3)
			$bar.css('backgroundColor',o.critical_color);		
		
		st = true;
		curt = notiftime[ind]*1000;
	
	
	
	}
	
	timeout = setTimeout(function() {
		
		if(st)
			writeannouncement((ind+1)% notif.length);
		else
			writeannouncement(0);
		
	},curt);
	
	
}


	});


}});});