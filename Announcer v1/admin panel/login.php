<?php 
require_once("../inc/config.php"); // includes the configuration file
	session_start();
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Realtime Announcer Login </title>

<link rel="stylesheet" type="text/css" href="css/login.css" />

</head>

<body>

	<div class="wrapper">
    	
        <div class="header">
        	<div> Realtime Announcer </div>
        </div>
        
        <form method="post" action="checklogin.php">
        	
            <label> Username: </label>
        	<input type="text" name="uname" />
            <label> Password: </label>
            <input type="password" name="upass" />
        
        	<input id="subm" type="submit" value="Login" />
        </form>
        
		<?php
			
			

			if(isset($_SESSION['incorrect']))
			{
				// show the Incorrect login message when the account is not found in the database
				echo '<p> Incorrect login </p>';
			}
		
		?>
        
    </div>

</body>

</html>