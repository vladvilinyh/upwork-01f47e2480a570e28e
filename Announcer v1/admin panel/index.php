<?php 
require_once("../inc/config.php"); // includes the configuration file
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Realtime Announcer Admin Panel </title>
<link rel="stylesheet" href="css/style.css" type="text/css" />

<script type="text/javascript">

	function valid()
	{
	
		var input = document.getElementById("uptnum").value;
		
    	if((input - 0) == input && input.length > 0 && input>0)
			return true;
		else
		{
			alert('invalid Up time, it has to be a number larger than 0');
			return false;	
		}

	}

</script>

</head>

<body>

	<div class="wrapper">
    
    <h1> RealTime Announcer </h1>
    
    	<div class="menu">
        
        <!--  the menu !-->
        	<a href="index.php"><div class="menubut" id="home"> <div> Current Announcements</div> </div> </a>
            <a href="index.php?add=true"><div class="menubut" id="add"> <div>New Announcement</div> </div> </a>
            <a href="index.php?changelog=true"><div class="menubut" id="change"> <div>Change Login Information</div>  </div> </a>
            <a href="logoff.php"><div class="menubut" id="logoff"> <div> Log Off</div> </div> </a>
            
        </div>
    
    	<div class="contents">
       
        
        	
                
            <?php

		
		  	// check if the user is not already logged in and send him back to the login page if neccessary
		  	if(!isset($_SESSION['user']))
				header('location:login.php');
			
			
			// the following conditions change the page contents according to the passed Get values
			if(isset($_GET['changelog']))
			{
				
				echo ' <h2> Change Login Details </h2>';
				
				echo '<form method="post" action"changeinfo.php class="chng">';
				
				echo '<label> Old UserName:  </label>';
				echo '<input type="text" name="olduname">';
				echo '<label> New UserName: </label>';
				echo '<input type="text" name="newuname">';
				echo '<label> Old Password: </label>';
				echo '<input type="text" name="oldupass">';
				echo '<label> New Password: </label>';
				echo '<input type="text" name="newupass">';
				
				echo '<input type="hidden" name="chng" value="true">';
				
				echo '<input id="submitb" type="submit" value="Submit" />';
				
				echo '</form>';
				
			}
			else if(isset($_GET['add']))
			{
				
				echo ' <h2> Add new Announcement </h2>';
				
				echo '<table class="tbl">';
				echo '<tr>';
				
				echo '<td>';
					echo '<form action="addnotif.php" method="post" onSubmit="return valid()">';
				echo '</td>';
				
				echo '<td>';
					echo 'Text : <textarea type="text" name="antext"></textarea>';
				echo '</td>';
				
				echo '<td>';
					echo 'Up time: <input type="text" name="anuptime" id="uptnum"/>';
				echo '</td>';
				
				echo '<td>';
					echo 'Importance : <SELECT NAME="animportance">';
						echo '<OPTION VALUE="1">Normal';
						echo '<OPTION VALUE="2">Moderate';
						echo '<OPTION VALUE="3">Critical';
					echo '</SELECT>';
				echo '</td>';
				
				echo '<td>';
					echo 'Active : <SELECT NAME="anactive">';
						echo '<OPTION VALUE="1">true';
						echo '<OPTION VALUE="0">false';
					echo '</SELECT>';
				echo '</td>';
				
				echo '<input type="hidden" name="addentry" value="true">';
				
				echo '<td>';
					echo 'Submit : <input type="submit" name="ansubmit" id="subm" value=" "/>';
				echo '</td>';
				
				
				echo '</form>';
				
				echo '</tr>';
			}
			else if(isset($_GET['edit']))
			{
				
				echo ' <h2> Edit Announcement </h2>';
				
				$ide = $_GET['edit'];
				
				$query = mysqli_query($con, "SELECT * from currentan where id=$ide");

				$res = mysqli_fetch_array($query);
				
				echo '<table class="tbl">';
				echo '<tr>';
				
				echo '<td>';
					echo '<form action="editnotif.php" method="post" onSubmit="return valid()">';
				echo '</td>';
				
				echo '<td>';
					echo 'Text : <textarea type="text" name="antext">'.$res['text'].'</textarea>';
				echo '</td>';
				
				echo '<td>';
					echo 'Up time: <input type="text" name="anuptime" id="uptnum" value="'. $res['Uptime'] .'"/>';
				echo '</td>';
				
				echo '<td>';
					echo 'Importance : <SELECT NAME="animportance">';
						echo '<OPTION VALUE="1"'; echo ($res['Importance'] == 1) ? 'selected="selected"' : '';  echo '>Normal';
						echo '<OPTION VALUE="2"'; echo ($res['Importance'] == 2) ? 'selected="selected"' : '';  echo'>Moderate';
						echo '<OPTION VALUE="3"'; echo ($res['Importance'] == 3) ? 'selected="selected"' : '';  echo'>Critical';
					echo '</SELECT>';
				echo '</td>';
				
				echo '<td>';
					echo 'Active : <SELECT NAME="anactive">';
						echo '<OPTION VALUE="1"'; echo ($res['active'] == 1) ? 'selected="selected"' : ''; echo '>true';
						echo '<OPTION VALUE="0"';  echo ($res['active'] == 0) ? 'selected="selected"' : '';  echo '>false';
					echo '</SELECT>';
				echo '</td>';
				
				echo '<input type="hidden" name="editentry" value="true">';
				echo '<input type="hidden" name="editid" value="'. $res['id'] .'" >';
				
				echo '<td>';
					echo 'Submit : <input type="submit" name="ansubmit" id="subm" value=" "/>';
				echo '</td>';
				
				
				echo '</form>';
				
				echo '</tr>';
			}
			else
			{
				
			echo ' <h2> Current News </h2>';
			echo ' <table class="tbl" >';
            echo ' <col width="30" />';
            echo ' <col width="300" />';
  			echo ' <col width="100" />';
  			echo ' <col width="150" />';
  			echo ' <col width="100" />';
            echo ' <col width="100" />';
            echo ' <col width="100" />';
			
            	echo '<tr>';
                	echo '<th id="empty">  </th>';
                    echo '<th> Announcement </th>';
                    echo '<th> Up time </th>';
                    echo '<th> Importance </th>';
                    echo '<th> Active </th>';
                    echo '<th> Edit </th>';
                    echo '<th> Delete </th>';
                echo '</tr>';
			
			// query to get the contents of the announcements from the database table
			$query = mysqli_query($con, "SELECT * from currentan");
			
			while($res = mysqli_fetch_array($query))
			{
			echo '<tr>';
			
				echo '<td>';
					echo '<img src="images/Comment.png"/>';
				echo '</td>';
				
				echo '<td> <p>';
					echo $res['text'];
				echo '</td>';
				
				echo '</p> <td>';
					echo '<span class="time">' . $res['Uptime'] . '</span>';
				echo '</td>';
				
				echo '<td>';
					echo ($res['Importance']==1) ? '<span class="norm"> Normal </span> ' : (($res['Importance']==2) ? '<span class="mod"> Moderate </span> ' : '<span class="crit"> Critical </span> ');
				echo '</td>';
				
				echo '<td>';
					echo ($res['active']==1) ? '<span class="mod"> ON </span>' : '<span class="crit"> OFF </span>';
				echo '</td>';
				
				echo '<td>';
					$tx = $res['id'];
					echo '<a href="index.php?edit=' . $tx . '"> <img src="images/Pencil.png"/> </a>';
				echo '</td>';
				
				echo '<td>';
					$tx = $res['id'];
					echo '<a href="deletenotif.php?delete=' . $tx . '"> <img src="images/Delete.png"/> </a>';
				echo '</td>';
				
			echo '</tr>';
			
			
			
			}
			

			
			}
			
			?>
            
            </table>
        
        </div>
    
    </div>

</body>


</html>